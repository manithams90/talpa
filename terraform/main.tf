##################
##  Namespaces ##
#################
resource "kubernetes_namespace" "this" {

  for_each = local.namespaces

  metadata {
    labels = merge(
      local.common_tags,
      try(each.value.labels, {})
    )
    name = each.key
  }
}

locals {
  common_tags = merge(
    {
      environment = var.environment
    },
    var.default_tags
  )
  namespaces = {
    "ingress-nginx" = {}
  }
}

# #################
# ## Helm Charts ##
# #################
# ingress-nginx
resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.10.0"

  namespace        = kubernetes_namespace.this["ingress-nginx"].id
  create_namespace = false

  values = [
    file("${path.module}/kubernetes_components/ingress-nginx/values.yaml")
  ]

  depends_on = [
    kubernetes_namespace.this
  ]
}